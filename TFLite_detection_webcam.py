######## Webcam Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 10/27/19
# Description: 
# This program uses a TensorFlow Lite model to perform object detection on a live webcam
# feed. It draws boxes and scores around the objects of interest in each frame from the
# webcam. To improve FPS, the webcam object runs in a separate thread from the main program.
# This script will work with either a Picamera or regular USB webcam.
#
# This code is based off the TensorFlow Lite image classification example at:
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/python/label_image.py
#
# I added my own method of drawing boxes and labels using OpenCV.

# Import packages
import os
import argparse
import cv2
import numpy as np
import sys
import time
from threading import Thread
import importlib.util
import requests
import json
import datetime

from PIL import ImageFont, ImageDraw, Image


API_URL = 'http://localhost:5000'
frames_counter = 0 # this variable is used to count frames
detection_interval = 50 # minimum number of frames between detections saved, to avoid multiple images that are frames following each other


def draw_label(image, text, x, y, color, font_size):
    # Draw label using PIL library, since opencv font selection is limited
    # Convert the image to RGB (OpenCV uses BGR)
    cv2_im_rgb = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)  
    
    # Pass the image to PIL  
    pil_im = Image.fromarray(cv2_im_rgb)  
    
    draw = ImageDraw.Draw(pil_im)  
    # use a truetype font  
    font = ImageFont.truetype("assets/Ubuntu-R.ttf", font_size)  
    
    # Draw the text  
    draw.text((x, y), text, font=font,fill=color)  
    
    # Get back the image to OpenCV  
    image = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGB2BGR)

    return image


# makes a post to the API that writes the data in the db
def write_to_db(endpoint, data_json):
    global frames_counter
    url = API_URL + '/' + endpoint
    
    if frames_counter > detection_interval:
        try:
            response = requests.post(url, json=data_json)
        except Exception as err:
            print(err)
            response = False
    else:
        response = False
    
    return response

# Get path to current working directory
CWD_PATH = os.getcwd()

# create image folders if they do not exist
if not os.path.exists(CWD_PATH + '/detections/'):
    os.makedirs('detections')

if not os.path.exists(CWD_PATH + '/detections/unreviewed'):
    os.makedirs('detections/unreviewed')

if not os.path.exists(CWD_PATH + '/detections/reviewed'):
    os.makedirs('detections/reviewed')

boundingbox_bgr = (80,83,239) # opencv uses BGR instead of RGB

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in',
                    required=True)
parser.add_argument('--graph', help='Name of the .tflite file, if different than detect.tflite',
                    default='detect.tflite')
parser.add_argument('--labels', help='Name of the labelmap file, if different than labelmap.txt',
                    default='labelmap.txt')
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects',
                    default=0.5)
parser.add_argument('--resolution', help='Desired webcam resolution in WxH. If the webcam does not support the resolution entered, errors may occur.',
                    default='1280x720')
parser.add_argument('--edgetpu', help='Use Coral Edge TPU Accelerator to speed up detection',
                    action='store_true')

args = parser.parse_args()

MODEL_NAME = args.modeldir
GRAPH_NAME = args.graph
LABELMAP_NAME = args.labels
min_conf_threshold = float(args.threshold)
resW, resH = args.resolution.split('x')
imW, imH = int(resW), int(resH)
use_TPU = args.edgetpu


# Import TensorFlow libraries
# Tries to import tflite_runtime, if not found then tries regular tensorflow
# If using Coral Edge TPU, import the load_delegate library
try:
    from tflite_runtime.interpreter import Interpreter
    if use_TPU:
        from tflite_runtime.interpreter import load_delegate
except ImportError:
    from tensorflow.lite.python.interpreter import Interpreter
    if use_TPU:
        from tensorflow.lite.python.interpreter import load_delegate
    print('tflite_runtime not installed therefore TensorFlow will be imported.')

# If using Edge TPU, assign filename for Edge TPU model
if use_TPU:
    # If user has specified the name of the .tflite file, use that name, otherwise use default 'edgetpu.tflite'
    if (GRAPH_NAME == 'detect.tflite'):
        GRAPH_NAME = 'edgetpu.tflite'       

# Path to .tflite file, which contains the model that is used for object detection
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,GRAPH_NAME)

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,MODEL_NAME,LABELMAP_NAME)

# Load the label map
with open(PATH_TO_LABELS, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

# Labels
if "mobilenet" in MODEL_NAME:
    # Have to do a weird fix for label map if using the COCO "starter model" first label is '???', which has to be removed.
    if labels[0] == '???':
        del(labels[0])
elif "item" in labels[0]:
    # convert labels list from TF(JSON) to TFLite(a class name per line)
    for label in list(labels):
        if "name" not in label:
            labels.remove(label)
        else:
            labels.remove(label)
            labels.append(label.replace("'","").replace("name: ",""))

# Load the Tensorflow Lite model.
# If using Edge TPU, use special load_delegate argument
if use_TPU:
    interpreter = Interpreter(model_path=PATH_TO_CKPT,
                              experimental_delegates=[load_delegate('libedgetpu.so.1.0')])
else:
    interpreter = Interpreter(model_path=PATH_TO_CKPT)

interpreter.allocate_tensors()

# Get model details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
height = input_details[0]['shape'][1]
width = input_details[0]['shape'][2]

floating_model = (input_details[0]['dtype'] == np.float32)

input_mean = 127.5
input_std = 127.5

# Define VideoStream class to handle streaming of video from webcam in separate processing thread
# Source - Adrian Rosebrock, PyImageSearch: https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/
class VideoStream:
    """Camera object that controls video streaming from the Picamera"""
    def __init__(self,resolution=(imW,imH),framerate=30):
        # Initialize the PiCamera and the camera image stream
        self.stream = cv2.VideoCapture(0)
        ret = self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        ret = self.stream.set(3,resolution[0])
        ret = self.stream.set(4,resolution[1])
            
        # Read first frame from the stream
        (self.grabbed, self.frame) = self.stream.read()

	# Variable to control when the camera is stopped
        self.stopped = False

    def start(self):
	# Start the thread that reads frames from the video stream
        Thread(target=self.update,args=()).start()
        return self

    def update(self):
        # Keep looping indefinitely until the thread is stopped
        while True:
            # If the camera is stopped, stop the thread
            if self.stopped:
                # Close camera resources
                self.stream.release()
                return

            # Otherwise, grab the next frame from the stream
            (self.grabbed, self.frame) = self.stream.read()

    def read(self):
	# Return the most recent frame
        return self.frame

    def stop(self):
	# Indicate that the camera and thread should be stopped
        self.stopped = True

# Initialize frame rate calculation
frame_rate_calc = 1
freq = cv2.getTickFrequency()

# Initialize video stream
videostream = VideoStream(resolution=(imW,imH),framerate=30).start()
time.sleep(1)

#for frame1 in camera.capture_continuous(rawCapture, format="bgr",use_video_port=True):
while True:
    frames_counter += 1
    timestamp = datetime.datetime.now()
    
    # Start timer (for calculating frame rate)
    t1 = cv2.getTickCount()

    # Grab frame from video stream
    frame1 = videostream.read()

    # Acquire frame and resize to expected shape [1xHxWx3]
    frame = frame1.copy()
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame_resized = cv2.resize(frame_rgb, (width, height))
    input_data = np.expand_dims(frame_resized, axis=0)

    # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
    if floating_model:
        input_data = (np.float32(input_data) - input_mean) / input_std

    # Perform the actual detection by running the model with the image as input
    interpreter.set_tensor(input_details[0]['index'],input_data)
    interpreter.invoke()

    # Retrieve detection results
    boxes = interpreter.get_tensor(output_details[0]['index'])[0] # Bounding box coordinates of detected objects
    classes = interpreter.get_tensor(output_details[1]['index'])[0] # Class index of detected objects
    scores = interpreter.get_tensor(output_details[2]['index'])[0] # Confidence of detected objects
    #num = interpreter.get_tensor(output_details[3]['index'])[0]  # Total number of detected objects (inaccurate and not needed)

    # Counts the number of objects currently in the frame
    if "count" in locals(): # checks if the variable is defined used to ignore the first frame
        previous_count = count
    else:
        previous_count = 0

    final_score = np.squeeze(scores)    
    count = 0
    for i in range(10):
        if scores is None or final_score[i] > 0.5:
                count = count + 1
    
    if count != previous_count and count >=1:
        frames_counter += detection_interval # will force this detection to be saved, ignoring the inverval (this detection is importante because the number of objects on the frame changed)

    object_detected = False
    # Loop over all detections and write detection to db if any confidence is above minimum threshold
    for i in range(len(scores)):
        if ((scores[i] > min_conf_threshold) and (scores[i] <= 1.0)):
            object_detected = True

    detection_response = False
    if object_detected and frames_counter > detection_interval:
        # Save the image to detections directory
        #date_time_obj = datetime.datetime.strptime(str(datetime.datetime.now()), '%Y-%m-%d %H:%M:%S.%f')
        date_time_obj = timestamp
        date_time_str = str(date_time_obj.time()).replace(':', 'h', 1).replace(':', 'm').replace('.','s')
        image_path = CWD_PATH + '/detections/unreviewed/' + str(date_time_obj.date()) + '_' + date_time_str + ".jpg"
        
        cv2.imwrite(image_path, frame)
        print('Detection saved at: ' + image_path)

        # save detection data and send it to the DB
        detection_data = {
            "image_path": image_path,
            "image_width": imW,
            "image_height": imH,
            "created_at": str(timestamp)
        }
        
        detection_response = write_to_db("detection", detection_data)
        
        if detection_response:
            inserted_detection_id = detection_response.json()['inserted_id']
        else:
            print("Cannot connect to the API(detection).")

    
    #cv2.putText(frame,'FPS: {0:.2f}'.format(frame_rate_calc),(30,50),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,0),2,cv2.LINE_AA)

    # Loop over all detections and draw detection box if confidence is above minimum threshold
    for i in range(len(scores)):
        if ((scores[i] > min_conf_threshold) and (scores[i] <= 1.0)):

            # Get bounding box coordinates and draw box
            # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
            ymin = int(max(1,(boxes[i][0] * imH)))
            xmin = int(max(1,(boxes[i][1] * imW)))
            ymax = int(min(imH,(boxes[i][2] * imH)))
            xmax = int(min(imW,(boxes[i][3] * imW)))
            
            cv2.rectangle(frame, (xmin,ymin), (xmax,ymax), boundingbox_bgr, 2)

            # Draw label rectangle
            object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
            accuracy = int(scores[i]*100)
            label = '%s: %d%%' % (object_name, accuracy) # Example: 'person: 72%'
            labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_TRIPLEX, 0.7, 2) # Get font size
            label_ymin = max(ymin, labelSize[1] + 10) # Make sure not to draw label too close to top of window
            cv2.rectangle(frame, (xmin-1, label_ymin-labelSize[1]+15), (xmin+labelSize[0]-1, label_ymin+baseLine+15), boundingbox_bgr, cv2.FILLED) # Draw white box to put label text in
            # cv2.putText(frame, label, (xmin, label_ymin+18), cv2.FONT_HERSHEY_TRIPLEX, 0.7, (0, 0, 0), 2) # Draw label text using opencv
            frame = draw_label(frame, label, xmin+3, label_ymin-5, (0,0,0,255), 25)

            if 'detection_response' in locals():
                if object_detected and detection_response != False and frames_counter > detection_interval:
                    # write object data to db
                    object_data = {
                        "detection_id": inserted_detection_id,
                        "class_name": object_name,
                        "accuracy": "0."+ str(accuracy),
                        "xmin": xmin,
                        "ymin": ymin,
                        "xmax": xmax,
                        "ymax": ymax,
                        "created_at": str(timestamp)
                    }
                    
                    object_response = write_to_db("object", object_data)
                    
                    if object_response:
                        inserted_object_id = object_response.json()['inserted_id']
                    else:
                        print("Cannot connect to the API(object).")

    if object_detected and frames_counter > detection_interval:
        # save image with bboxes to a file
        image_path = CWD_PATH + '/detections/unreviewed/' + str(date_time_obj.date()) + '_' + date_time_str + "_BBOXES.jpg"
        cv2.imwrite(image_path, frame)

    # draw fps counter after saving the image with the bboxes
    cv2.rectangle(frame, (20,20), (97, 45), (0,0,0), cv2.FILLED) # FPS background box
    cv2.rectangle(frame, (20,20), (97, 45), (255,255,255), 1) # border for FPS background box 
    frame = draw_label(frame, 'FPS: {0:.2f}'.format(frame_rate_calc), 25, 23, (255,255,255,255), 15)
    
    # resets interval between detections that are saved
    if object_detected and detection_response != False:
        frames_counter = 0

    # All the results have been drawn on the frame, so it's time to display it.
    cv2.imshow('Object detector', frame)

    # Calculate framerate
    t2 = cv2.getTickCount()
    time1 = (t2-t1)/freq
    frame_rate_calc= 1/time1


    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
cv2.destroyAllWindows()
videostream.stop()
