TODO: Blue and dark images run 100k and 200k
TODO: Try floating object detection on Red channel of images (all classes renamed to floating object)

**TensorFlow Lite object detection**

This repository contains several example scripts running TensorFlow's object detection.

Executing the script:
The **MODEL_FOLDER** should contain 2 files named: detect.tflite and labelmap.txt

**Coral DevBoard depencendies:**
Download latest Mendel OS: https://coral.ai/software/

Install OS following Dev Board Setup Guide: https://medium.com/@aallan/hands-on-with-the-coral-dev-board-adbcc317b6af

While still connected through screen or mdt shell enable ssh access by editing sshd_config:

```
sudo nano /etc/ssh/sshd_config (change to PermitRootLogin yes , PasswordAuthentication yes )
```

Run install_requirements.sh:

```
bash install_requirements.sh
```

**Raspberry Pi dependencies:**

```
pip install opencv-python
```

**Common Dependencies**:
Install tflite_runtime: https://www.tensorflow.org/lite/guide/python

To access the IoT device remotely you can install NoMachine (there are different versions for each device, one for RPi and one for Coral DevBoard) from https://www.nomachine.com/download.

**To run TensorFlow's object detection in images and save the result in a folder called detections:**

-For a single image:

```
    python TFLite_detection_image.py --modeldir=MODEL_FOLDER --image=IMAGE_FILE
```

-For multiple images inside a directory:

```
    python TFLite_detection_image.py --modeldir=MODEL_FOLDER --imagedir=IMAGES_FOLDER
```

**To run TensorFlow's object detection in a video and play it with labels and bounding boxes:**

```
    python TFLite_detection_image.py --modeldir=MODEL_FOLDER --video=VIDEO_FILE
```

**To run TensorFlow's object detection in real time by using a connected camera:**

```
    python TFLite_detection_image.py --modeldir=MODEL_FOLDER
```

Other arguments that can be added (as seen in the begging of the code):

```
--modeldir - Folder the .tflite file is located in
             required=True
```

```
--graph    - Name of the .tflite file, if different than detect.tflite
             default='detect.tflite'
```

```
--labels   - Name of the labelmap file, if different than labelmap.txt
             default='labelmap.txt'
```

```
--threshold- Minimum confidence threshold for displaying detected objects
             default=0.5
```

```
--image**    - Name of the single image to perform detection on. To run detection on multiple images, use --imagedir**
             default=None
```

```
--imagedir** - Name of the folder containing images to perform detection on. Folder must contain only images.
             default=None
```

```
--edgetpu  - Use Coral Edge TPU Accelerator to speed up detection
             action='store_true'
```

**To run the scripts with edgetpu it needs to be using tflite_runtime (https://www.tensorflow.org/lite/guide/python) instead of tensorflow.lite and the edgetpu library needs to be installed (https://coral.ai/docs/accelerator/get-started/).**
