# Run command example: sudo python3 /home/diogomartins/Downloads/tflite1/TFLite_detection_image.py --modeldir=Trained_model/ --imagedir=images

######## Webcam Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 9/28/19
# Description: 
# This program uses a TensorFlow Lite object detection model to perform object 
# detection on an image or a folder full of images. It draws boxes and scores 
# around the objects of interest in each image.
#
# This code is based off the TensorFlow Lite image classification example at:
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/python/label_image.py
#
# I added my own method of drawing boxes and labels using OpenCV.

# Import packages
import os
import argparse
import cv2
import numpy as np
import sys
import glob
import importlib.util

from PIL import ImageFont, ImageDraw, Image

boundingbox_bgr = (80,83,239) # opencv uses BGR instead of RGB

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in',
                    required=True)
parser.add_argument('--graph', help='Name of the .tflite file, if different than detect.tflite',
                    default='detect.tflite')
parser.add_argument('--labels', help='Name of the labelmap file, if different than labelmap.txt',
                    default='labelmap.txt')
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects',
                    default=0.5)
parser.add_argument('--image', help='Name of the single image to perform detection on. To run detection on multiple images, use --imagedir',
                    default=None)
parser.add_argument('--imagedir', help='Name of the folder containing images to perform detection on. Folder must contain only images.',
                    default=None)
parser.add_argument('--edgetpu', help='Use Coral Edge TPU Accelerator to speed up detection',
                    action='store_true')

args = parser.parse_args()

MODEL_NAME = args.modeldir
GRAPH_NAME = args.graph
LABELMAP_NAME = args.labels
min_conf_threshold = float(args.threshold)
use_TPU = args.edgetpu

# Parse input image name and directory. 
IM_NAME = args.image
IM_DIR = args.imagedir

# If both an image AND a folder are specified, throw an error
if (IM_NAME and IM_DIR):
    print('Error! Please only use the --image argument or the --imagedir argument, not both. Issue "python TFLite_detection_image.py -h" for help.')
    sys.exit()

# If neither an image or a folder are specified, default to using 'test1.jpg' for image name
if (not IM_NAME and not IM_DIR):
    IM_NAME = 'test1.jpg'

# Import TensorFlow libraries
# Tries to import tflite_runtime, if not found then tries regular tensorflow
# If using Coral Edge TPU, import the load_delegate library
try:
    from tflite_runtime.interpreter import Interpreter
    if use_TPU:
        from tflite_runtime.interpreter import load_delegate
except ImportError:
    from tensorflow.lite.python.interpreter import Interpreter
    if use_TPU:
        from tensorflow.lite.python.interpreter import load_delegate
    print('tflite_runtime not installed therefore TensorFlow will be imported.')

# If using Edge TPU, assign filename for Edge TPU model
if use_TPU:
    # If user has specified the name of the .tflite file, use that name, otherwise use default 'edgetpu.tflite'
    if (GRAPH_NAME == 'detect.tflite'):
        GRAPH_NAME = 'edgetpu.tflite'


# Get path to current working directory
CWD_PATH = os.getcwd()

# Define path to images and grab all image filenames
if IM_DIR:
    PATH_TO_IMAGES = os.path.join(CWD_PATH,IM_DIR)
    images = glob.glob(PATH_TO_IMAGES + '/*')

elif IM_NAME:
    PATH_TO_IMAGES = os.path.join(CWD_PATH,IM_NAME)
    images = glob.glob(PATH_TO_IMAGES)

# Path to .tflite file, which contains the model that is used for object detection
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,GRAPH_NAME)

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,MODEL_NAME,LABELMAP_NAME)

# Load the label map
with open(PATH_TO_LABELS, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

# Labels
if "mobilenet" in MODEL_NAME:
    # Have to do a weird fix for label map if using the COCO "starter model" first label is '???', which has to be removed.
    if labels[0] == '???':
        del(labels[0])
elif "item" in labels[0]:
    # convert labels list from TF(JSON) to TFLite(a class name per line)
    for label in list(labels):
        if "name" not in label:
            labels.remove(label)
        else:
            labels.remove(label)
            labels.append(label.replace("'","").replace("name: ",""))

# Load the Tensorflow Lite model.
# If using Edge TPU, use special load_delegate argument
if use_TPU:
    interpreter = Interpreter(model_path=PATH_TO_CKPT,
                              experimental_delegates=[load_delegate('libedgetpu.so.1.0')])
else:
    interpreter = Interpreter(model_path=PATH_TO_CKPT)

interpreter.allocate_tensors()

# Get model details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
height = input_details[0]['shape'][1]
width = input_details[0]['shape'][2]

floating_model = (input_details[0]['dtype'] == np.float32)

input_mean = 127.5
input_std = 127.5

# Remove directories from image list to prevent errors when trying to perform object detection on a folder
for image_path in images:
    if os.path.isdir(image_path):
        images.remove(image_path)

# Loop over every image and perform detection
for image_path in images:
    # Load image and resize to expected shape [1xHxWx3]
    image = cv2.imread(image_path)
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    imH, imW, _ = image.shape 
    image_resized = cv2.resize(image_rgb, (width, height))
    input_data = np.expand_dims(image_resized, axis=0)

    # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
    if floating_model:
        input_data = (np.float32(input_data) - input_mean) / input_std

    # Perform the actual detection by running the model with the image as input
    interpreter.set_tensor(input_details[0]['index'],input_data)
    interpreter.invoke()

    # Retrieve detection results
    boxes = interpreter.get_tensor(output_details[0]['index'])[0] # Bounding box coordinates of detected objects
    classes = interpreter.get_tensor(output_details[1]['index'])[0] # Class index of detected objects
    scores = interpreter.get_tensor(output_details[2]['index'])[0] # Confidence of detected objects
    #num = interpreter.get_tensor(output_details[3]['index'])[0]  # Total number of detected objects (inaccurate and not needed)

    # Loop over all detections and draw detection box if confidence is above minimum threshold
    for i in range(len(scores)):
        if ((scores[i] > min_conf_threshold) and (scores[i] <= 1.0)):

            # Get bounding box coordinates and draw box
            # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
            ymin = int(max(1,(boxes[i][0] * imH)))
            xmin = int(max(1,(boxes[i][1] * imW)))
            ymax = int(min(imH,(boxes[i][2] * imH)))
            xmax = int(min(imW,(boxes[i][3] * imW)))
            
            cv2.rectangle(image, (xmin,ymin), (xmax,ymax), boundingbox_bgr , 2)

            # Draw label rectangle
            object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
            label = '%s: %d%%' % (object_name, int(scores[i]*100)) # Example: 'person: 72%'
            labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_TRIPLEX, 0.7, 2) # Get font size
            label_ymin = max(ymin, labelSize[1] + 10) # Make sure not to draw label too close to top of window
            cv2.rectangle(image, (xmin-1, label_ymin-labelSize[1]+15), (xmin+labelSize[0]-1, label_ymin+baseLine+15), boundingbox_bgr, cv2.FILLED) # Draw white box to put label text in
            # cv2.putText(image, label, (xmin, label_ymin+18), cv2.FONT_HERSHEY_TRIPLEX, 0.7, (0, 0, 0), 2) # Draw label text using opencv
            
            # Draw label using PIL library, since opencv font selection is limited
            # Convert the image to RGB (OpenCV uses BGR)  
            cv2_im_rgb = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)  
            
            # Pass the image to PIL  
            pil_im = Image.fromarray(cv2_im_rgb)  
            
            draw = ImageDraw.Draw(pil_im)  
            # use a truetype font  
            font = ImageFont.truetype("assets/Ubuntu-R.ttf", 25)  
            
            # Draw the text  
            draw.text((xmin+3, label_ymin-5), label, font=font,fill=(0,0,0,255))  
            
            # Get back the image to OpenCV  
            image = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGB2BGR)

    # All the results have been drawn on the image, displays the image in a window
    #cv2.imshow('Object detector', image)

    # Save the image to detections directory
    detection_path = os.path.join(CWD_PATH, 'detections')
    detection_images_path = os.path.join(detection_path, 'images')
    if not os.path.exists(detection_path):
        os.makedirs(detection_path)
    
    if not os.path.exists(detection_images_path):
        os.makedirs(detection_images_path)

    file_name = os.path.basename(image_path) # extracts the filename from the image_path
    cv2.imwrite(os.path.join(detection_images_path, file_name), image) # TODO: FIX all paths using OS library instead of string concats (this works on linux but not on windows)
    print('Detection saved at: ' + os.path.join(detection_images_path, file_name))

    # Press any key to continue to next image, or press 'q' to quit
    if cv2.waitKey(0) == ord('q'):
        break

# Clean up
cv2.destroyAllWindows()
