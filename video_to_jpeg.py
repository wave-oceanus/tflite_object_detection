import cv2
import argparse
import os
import time
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--video', required=True)
parser.add_argument('--dir',required=True)
args = parser.parse_args()
video = args.video
dir = args.dir

if not os.path.exists(dir):
  os.makedirs(dir)

vidcap = cv2.VideoCapture(video)
length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
success,image = vidcap.read()

for i in tqdm(range(length)):
  cv2.imwrite(str(dir)+"/frame%d.jpg" % i, image)     # save frame as JPEG file
  success,image = vidcap.read()
  
