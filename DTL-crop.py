# This script generates all the crop actions to generate multiple images near the optimal size of "minimumTargetResolution" from the original size if it is too big for training (eg: 4k images cropped to 300x300 chunks)
# Run "beautify file" on the resulting json

originalResolution = [5472, 3648]
minimumTargetResolution = 300

targetResolution = []

def getLowestCommonDivisor(width, height, start):
    for i  in range(start, height):
        if height % i == 0:
            if width % (height / i) == 0:
                return height / i

lowestCommonDivisor = int(getLowestCommonDivisor(originalResolution[0], originalResolution[1], minimumTargetResolution))
targetResolution = [int(originalResolution[0] / lowestCommonDivisor), int(originalResolution[1] / lowestCommonDivisor)]

file = open("cropDTL.json", "w+") 
file.write("[")

for i in range (0, lowestCommonDivisor):
    left = originalResolution[0] - targetResolution[0] * (12 - i)
    right = originalResolution[0] - targetResolution[0] * (i + 1)
    for j in range (0, lowestCommonDivisor):
        top = originalResolution[1] - targetResolution[1] * (12 - j)
        bottom = originalResolution[1] - targetResolution[1] * (j +1)

        cropString = ("{"\
                    "\"action\": \"crop\", "\
                    "\"src\": ["\
                    "\"$data\""\
                    "],"\
                    "\"dst\": \"$cropped\","\
                    "\"settings\": {"\
                    "\"sides\": {"\
                    "\"left\": \"%dpx\","\
                    "\"top\": \"%dpx\","\
                    "\"right\": \"%dpx\","\
                    "\"bottom\": \"%dpx\""\
                    "}"\
                    "}"\
                    "}," % (left, top, right, bottom))
        file.write(cropString)

file.write("]")

file.close