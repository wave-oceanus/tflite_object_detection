######## Webcam Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 10/2/19
# Description: 
# This program uses a TensorFlow Lite model to perform object detection on a
# video. It draws boxes and scores around the objects of interest in each frame
# from the video.
#
# This code is based off the TensorFlow Lite image classification example at:
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/python/label_image.py
#
# I added my own method of drawing boxes and labels using OpenCV.

# Import packages
import io
import os
import argparse
import cv2
import numpy as np
import sys
import importlib.util
import statistics

from random import randint
import csv
import xlsxwriter
import collections
from PIL import ImageFont, ImageDraw, Image
 

def Average(lst): 
    if len(lst) > 0:
        return sum(lst) / len(lst)
    else:
        return 0

def standard_dev(lst):
    if len(lst) >= 2:
        return statistics.stdev(lst)
    else:
        return 0

def draw_label(image, text, x, y, color, font_size):
    # Draw label using PIL library, since opencv font selection is limited
    # Convert the image to RGB (OpenCV uses BGR)  
    cv2_im_rgb = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)  
    
    # Pass the image to PIL  
    pil_im = Image.fromarray(cv2_im_rgb)  
    
    draw = ImageDraw.Draw(pil_im)  
    # use a truetype font  
    font = ImageFont.truetype("assets/Ubuntu-R.ttf", font_size)  
    
    # Draw the text  
    draw.text((x, y), text, font=font,fill=color)  
    
    # Get back the image to OpenCV  
    image = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGB2BGR)

    return image


saved_values = {}
counter_detections = {}
fps_values = [] 

boundingbox_bgr = (80,83,239) # opencv uses BGR instead of RGB

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in',
                    required=True)
parser.add_argument('--graph', help='Name of the .tflite file, if different than detect.tflite',
                    default='detect.tflite')
parser.add_argument('--labels', help='Name of the labelmap file, if different than labelmap.txt',
                    default='labelmap.txt')
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects',
                    default=0.5)
parser.add_argument('--video', help='Name of the video file',
                    default='test.mp4')
parser.add_argument('--edgetpu', help='Use Coral Edge TPU Accelerator to speed up detection',
                    action='store_true')

args = parser.parse_args()

MODEL_NAME = args.modeldir
GRAPH_NAME = args.graph
LABELMAP_NAME = args.labels
VIDEO_NAME = args.video
min_conf_threshold = float(args.threshold)
use_TPU = args.edgetpu

# Import TensorFlow libraries
# Tries to import tflite_runtime, if not found then tries regular tensorflow
# If using Coral Edge TPU, import the load_delegate library
try:
    from tflite_runtime.interpreter import Interpreter
    if use_TPU:
        from tflite_runtime.interpreter import load_delegate
except ImportError:
    print('tflite_runtime not installed therefore TensorFlow will be imported.')
    from tensorflow.lite.python.interpreter import Interpreter
    if use_TPU:
        from tensorflow.lite.python.interpreter import load_delegate
    

# If using Edge TPU, assign filename for Edge TPU model
if use_TPU:
    # If user has specified the name of the .tflite file, use that name, otherwise use default 'edgetpu.tflite'
    if (GRAPH_NAME == 'detect.tflite'):
        GRAPH_NAME = 'edgetpu.tflite'   

# Get path to current working directory
CWD_PATH = os.getcwd()

# Path to video file
VIDEO_PATH = os.path.join(CWD_PATH,VIDEO_NAME)

# Path to .tflite file, which contains the model that is used for object detection
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,GRAPH_NAME)

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,MODEL_NAME,LABELMAP_NAME)

# Load the label map
with open(PATH_TO_LABELS, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

# Labels
if "mobilenet" in MODEL_NAME:
    # Have to do a weird fix for label map if using the COCO "starter model" first label is '???', which has to be removed.
    if labels[0] == '???':
        del(labels[0])
elif "item" in labels[0]:
    # convert labels list from TF(JSON) to TFLite(a class name per line)
    for label in list(labels):
        if "name" not in label:
            labels.remove(label)
        else:
            labels.remove(label)
            labels.append(label.replace("'","").replace("name: ",""))


# Load the Tensorflow Lite model.
# If using Edge TPU, use special load_delegaqte argument
if use_TPU:
    interpreter = Interpreter(model_path=PATH_TO_CKPT,
                              experimental_delegates=[load_delegate('libedgetpu.so.1.0')])
else:
    interpreter = Interpreter(model_path=PATH_TO_CKPT)

interpreter.allocate_tensors()

# Get model details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
height = input_details[0]['shape'][1]
width = input_details[0]['shape'][2]

floating_model = (input_details[0]['dtype'] == np.float32)

input_mean = 127.5
input_std = 127.5

# Open video file
video = cv2.VideoCapture(VIDEO_PATH)
video.set(35,90)
imW = video.get(cv2.CAP_PROP_FRAME_WIDTH)
imH = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
 
# Used to save the result video to a file
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 15, (int(imW), int(imH)))

# Initialize frame rate calculation
frame_rate_calc = 1
freq = cv2.getTickFrequency()

while(video.isOpened()):
    # Start timer (for calculating frame rate)
    t1 = cv2.getTickCount()

    # Acquire frame and resize to expected shape [1xHxWx3]
    ret, frame = video.read()

    if ret:
        #frame = cv2.flip(frame, 0) # to flip the video if necessary
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame_resized = cv2.resize(frame_rgb, (width, height))
        input_data = np.expand_dims(frame_resized, axis=0)

        # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
        if floating_model:
            input_data = (np.float32(input_data) - input_mean) / input_std

        # Perform the actual detection by running the model with the image as input
        interpreter.set_tensor(input_details[0]['index'],input_data)
        interpreter.invoke()

        # Retrieve detection results
        boxes = interpreter.get_tensor(output_details[0]['index'])[0] # Bounding box coordinates of detected objects
        classes = interpreter.get_tensor(output_details[1]['index'])[0] # Class index of detected objects
        scores = interpreter.get_tensor(output_details[2]['index'])[0] # Confidence of detected objects
        #num = interpreter.get_tensor(output_details[3]['index'])[0]  # Total number of detected objects (inaccurate and not needed)

        # Calculate framerate
        t2 = cv2.getTickCount()
        time1 = (t2-t1)/freq
        frame_rate_calc= 1/time1
        fps_values.append(frame_rate_calc)
                
        cv2.rectangle(frame, (20,20), (97, 45), (0,0,0), cv2.FILLED) # FPS background box
        cv2.rectangle(frame, (20,20), (97, 45), (255,255,255), 1) # border for FPS background box 
        frame = draw_label(frame, 'FPS: {0:.2f}'.format(frame_rate_calc), 25, 23, (255,255,255,255), 15)
        #cv2.putText(frame,'FPS: {0:.2f}'.format(frame_rate_calc),(30,50),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,0),2,cv2.LINE_AA)

        # Loop over all detections and draw detection box if confidence is above minimum threshold
        for i in range(len(scores)):
            if ((scores[i] > min_conf_threshold) and (scores[i] <= 1.0)):

                # Get bounding box coordinates and draw box
                # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
                ymin = int(max(1,(boxes[i][0] * imH)))
                xmin = int(max(1,(boxes[i][1] * imW)))
                ymax = int(min(imH,(boxes[i][2] * imH)))
                xmax = int(min(imW,(boxes[i][3] * imW)))

                cv2.rectangle(frame, (xmin,ymin), (xmax,ymax), boundingbox_bgr, 4)

                # Draw label rectangle
                object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
                label = '%s: %d%%' % (object_name, int(scores[i]*100)) # Example: 'person: 72%'
                labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_TRIPLEX, 0.7, 2) # Get font size
                label_ymin = max(ymin, labelSize[1] + 10) # Make sure not to draw label too close to top of window
                cv2.rectangle(frame, (xmin-1, label_ymin-labelSize[1]+15), (xmin+labelSize[0]-1, label_ymin+baseLine+15), boundingbox_bgr, cv2.FILLED) # Draw box to put label text in
                # cv2.putText(frame, label, (xmin, label_ymin+18), cv2.FONT_HERSHEY_TRIPLEX, 0.7, (0, 0, 0), 2) # Draw label text using opencv
                frame = draw_label(frame, label, xmin+3, label_ymin-5, (0,0,0,255), 25)
                
                # saves values to get an average at the end
                if object_name not in counter_detections.keys(): #adds class to dictionary
                    counter_detections[object_name] = 0
                counter_detections[object_name] += 1

                if object_name not in saved_values.keys(): #adds class to dictionary
                    saved_values[object_name] = []
                saved_values[object_name].append(int(scores[i]*100))

        # All the results have been drawn on the frame, so it's time to display it.
        cv2.imshow('Object detector', frame)

        out.write(frame)

        # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            break
        
        # print calculations results
        # print("Averages:")
        # for key in saved_values:
        #     print(str(key) + ": " + str(Average(saved_values[key])))

        # print("")
        # print("SD:")
        # for key in saved_values:
        #     print(str(key) + ": " + str(standard_dev(saved_values[key])))

        # print("")
        # print("Detections:")
        # for key in counter_detections:
        #     print(str(key) + ": " + str(counter_detections[key]))

        # print("")
        # print("Avg FPS: ")
        # print(str(sum(fps_values)/len(fps_values)))
    else:
        break

# write the resulting data to a xlsx file

# orders the results alphabeticaly
od_counter_detections = collections.OrderedDict(sorted(counter_detections.items()))
od_saved_values = collections.OrderedDict(sorted(saved_values.items()))

# generate filename
results_filename = VIDEO_NAME.split(".")[0].split("/")[1] + ".xlsx"
print("Results file: " + results_filename)
workbook = xlsxwriter.Workbook(results_filename) 
worksheet = workbook.add_worksheet() 

worksheet.write('A1', 'Specie')
worksheet.write('B1', 'Detections(Frames)')
worksheet.write('C1', 'Average accuracy')
worksheet.write('D1', 'SD')

worksheet.write('F1', 'Average FPS:')
worksheet.write('G1', str(sum(fps_values)/len(fps_values)))

row = 1
for key in od_saved_values: 
    worksheet.write(row, 0, str(key)) 
    worksheet.write(row, 1, str(counter_detections[key])) 
    worksheet.write(row, 2, str(Average(saved_values[key]))) 
    worksheet.write(row, 3, str(standard_dev(saved_values[key]))) 

    row += 1

workbook.close() 

# Clean up
video.release()
out.release()
cv2.destroyAllWindows()