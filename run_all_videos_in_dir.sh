#!/bin/bash

# sysinfo_page - A script to run TFLite_object_detection_video.py on all the files in a given directory.

# Run example: bash run_all_videos_in_dir.sh /directory/here

declare -a videos_list

for entry in "$1"/*
do
  if [[ -f "$entry" ]]; then
        #echo "$entry"
        videos_list+="$entry"
        echo "$entry"
        python TFLite_detection_video.py --threshold=0.6 --modeldir=OceanusNet_1k/ --video="$entry"
    fi
done
