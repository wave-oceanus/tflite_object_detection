from flask import Flask, jsonify, request, send_file
from flask_restful import reqparse, abort, Api, Resource
import mysql.connector
from datetime import datetime
import json
import random
import logging
import os, sys
import simplejson
from PIL import Image

os.chdir(sys.path[0]) #changes working directory to write the log file in the same folder as the script

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

api = Api(app)

connection_config = {
    'host':'localhost',
    'user':'app',
    'passwd':'oceanus2020app',
    'database':'oceanus'
}

classes_dict = {}

def get_connection():
    try:
        mydb = mysql.connector.connect(**connection_config)
        if (mydb.is_connected() is False): #If connection exists leave Try statement
            logging.info("MYSQL: Trying to connect to the database.")
            mydb = mysql.connector.connect(**connection_config)
            logging.info("MYSQL: Database connection was successfull")
    except Exception as err:
        print("MYSQL: {0}".format(err))
        return False
    finally:
        if mydb.is_connected() is True: #If connection exists leave Try statement
            return mydb


# get existing classes in the database and save them in a global variable
def get_classes_list():
    myconn = get_connection()
    mycursor = myconn.cursor()
    mycursor.execute("SELECT id, name "\
                     "FROM class ")
    myresult = mycursor.fetchall()

    row_headers=[x[0] for x in mycursor.description] #this will extract row headers

    for result in myresult:
        classes_dict.update( {result[0] :  result[1]} )

    return True


# returns the class id associated with the received name
def get_class_id(name):
    for key, value in classes_dict.items():
        if value == name:
            return key
    
    return False


def row_exists(myresult):
    if len(myresult) == 0:
        return False

    return True

def resize_image(image_path, newWidth, newHeight):
    image = Image.open(image_path).convert('RGB')
    new_image = image.resize((newWidth, newHeight))
    new_path = image_path.replace("unreviewed", "thumbnails")
    
    # create thumbnails folders if it does not exist
    if not os.path.exists(os.path.dirname(new_path)):
        os.makedirs(os.path.dirname(new_path))

    new_image.save(new_path) # thumbnails folder must exist for it so save
    return new_path
    
    
############################
# Index
############################
class Index(Resource):
    def get(self):
        return "API is running."


############################
# Class
############################
class ClassList(Resource):
    # returns a list of all classes, and lets you POST to add new classes
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT c.id AS id, c.name AS name, c.created_at AS created_at "\
            "FROM class c "\
            "ORDER BY c.id ASC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new class
    def post(self):
        data = request.get_json()

        # Generates a list of missing values in the request to return
        missing_values = []
        if 'name' not in data: missing_values.append("name")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            sql = "INSERT INTO class (name) VALUES ('%s')"
            val = (data['name'])

            try:
                mycursor.execute(sql % val)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class ClassByID(Resource):
    # returns a single class item
    def get(self, id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT c.id AS id, c.name AS name, c.created_at AS created_at "\
            "FROM class c "\
            "WHERE c.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Class " + str(id) + " doesn't exist."}

        return jsonify(response)


############################
# Detection
############################
class DetectionList(Resource):
    # returns a list of all detections, and lets you POST to add new detection
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT d.id, d.image_path, d.lat AS latitude, d.lon AS longitude, COUNT(o.id) as objects, d.user_confirmation, d.comment, d.created_at AS created_at "\
            "FROM detection d "\
            "JOIN object o ON d.id=o.detection_id "\
            "GROUP BY d.id "\
            "ORDER BY d.id DESC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new detection
    def post(self):
        data = request.get_json()
        
        # Generates a list of missing values in the request to return
        missing_values = []
        if 'image_path' not in data: missing_values.append("image_path")
        if 'image_width' not in data: missing_values.append("image_width")
        if 'image_height' not in data: missing_values.append("image_height")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            # generates string of columns and values to insert into the db
            columns_received = ""
            values_received = ""
            for key, value in data.items():
                columns_received += key + ", "
                values_received += "\"" + str(value) + "\", "
            columns_received = columns_received[:-2] # removes the final ","
            values_received = values_received[:-2]   # removes the final ","

            myconn = get_connection()
            mycursor = myconn.cursor()

            sql = "INSERT INTO detection (" + columns_received + ") VALUES (" + values_received + ")"

            try:
                mycursor.execute(sql)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)

    # Updates detection with the user_confirmation and comment
    def put(self):
        data = request.get_json()
        
        myconn = get_connection()
        mycursor = myconn.cursor()

        sql = "UPDATE detection SET user_confirmation=%s, comment=%s WHERE id=%s"
        val = (data["user_confirmation"], data["comment"], data["detection_id"])

        insert_success = True
        error_message = ""

        try:
            mycursor.execute(sql, val)
            myconn.commit()
            # get updated visit data
            mycursor.execute("SELECT d.id, d.image_path, d.lat AS latitude, d.lon AS longitude, d.user_confirmation, d.comment, d.created_at AS created_at "\
                                "FROM detection d "\
                                "WHERE d.id = " + str(data["detection_id"]) + ";")

            myresult = mycursor.fetchall()

            row_headers=[x[0] for x in mycursor.description] #this will extract row headers

            response_data=[]
            for result in myresult:
                response_data.append(dict(zip(row_headers,result)))
        except Exception as err:
            insert_success = False
            error_message += "MYSQL: {0}".format(err)

        
        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'result': "Updated " + str(mycursor.rowcount) + " row(s).",
                        'data': response_data}
            logging.info("%s record(s) updated.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class DetectionListUnreviewed(Resource):
    # returns a list of all detections, and lets you POST to add new detection
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT d.id, d.image_path, d.lat AS latitude, d.lon AS longitude, COUNT(o.id) as objects, d.user_confirmation, d.comment, d.created_at AS created_at "\
            "FROM detection d "\
            "JOIN object o ON d.id=o.detection_id "\
            "WHERE user_confirmation is NULL "\
            "GROUP BY d.id "\
            "ORDER BY d.id DESC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)


class DetectionThumbnailByID(Resource):
    # returns a detection image
    def get(self, id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT d.id AS id, d.image_path, d.lat AS lat, d.lon AS lon, d.created_at AS created_at "\
            "FROM detection d "\
            "WHERE d.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = send_file(resize_image(data[0]['image_path'], 100, 100))
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Detection " + str(id) + " doesn't exist."}

        return response
    

class DetectionByID(Resource):
    # returns a detection image
    def get(self, id, raw):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT d.id AS id, d.image_path, d.lat AS lat, d.lon AS lon, d.created_at AS created_at "\
            "FROM detection d "\
            "WHERE d.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            if raw == 0:
                response = send_file(data[0]['image_path'])
            else:
                response = send_file(data[0]['image_path'].replace(".jpg","_BBOXES.jpg"))
            # response = {'success': True,
            #             'timestamp': str(datetime.now()),
            #             'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Detection " + str(id) + " doesn't exist."}

        return response

############################
# Object
############################
class ObjectList(Resource):
    # returns a list of all objects, and lets you POST to add new object
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT o.detection_id, d.created_at as time, o.class_id, c.name as class_name, o.accuracy "\
                            "FROM object as o "\
                            "JOIN detection d ON o.detection_id=d.id "\
                            "JOIN class c ON o.class_id=c.id")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new object
    def post(self):
        data = request.get_json()
        
        # Generates a list of missing values in the request to return
        missing_values = []
        if 'detection_id' not in data: missing_values.append("detection_id")
        if 'class_name' not in data: missing_values.append("class_name")
        if 'accuracy' not in data: missing_values.append("accuracy")
        if 'xmin' not in data: missing_values.append("xmin")
        if 'ymin' not in data: missing_values.append("ymin")
        if 'xmax' not in data: missing_values.append("xmax")
        if 'ymax' not in data: missing_values.append("ymax")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            class_exists = ""
            # generates string of columns and values to insert into the db
            columns_received = ""
            values_received = ""
            for key, value in data.items():
                # replaces the class name with the correct id
                if key == "class_name":
                    key = "class_id"
                    value = get_class_id(value)
                    class_exists = value
                
                columns_received += key + ", "
                values_received += "\"" + str(value) + "\", "

            columns_received = columns_received[:-2] # removes the final ","
            values_received = values_received[:-2]   # removes the final ","

            if class_exists != False:
                myconn = get_connection()
                mycursor = myconn.cursor()

                sql = "INSERT INTO object (" + columns_received + ") VALUES (" + values_received + ")"
            
                try:
                    mycursor.execute(sql)
                    myconn.commit()
                except Exception as err:
                    insert_success = False
                    error_message += "MYSQL: {0}".format(err)

        if "class_exists" in locals() and class_exists == False:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Class is invalid"}
        elif (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


# returns a single object item
class ObjectByID(Resource):
    def get(self, id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT o.id, o.detection_id, d.created_at as time, o.class_id, c.name as class_name, o.accuracy "\
                            "FROM object as o "\
                            "JOIN detection d ON o.detection_id=d.id "\
                            "JOIN class c ON o.class_id=c.id "\
                            "WHERE o.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Object " + str(id) + " doesn't exist."}

        return jsonify(response)


# returns all objects associated with the detection_id
class ObjectByDetectionID(Resource):
    def get(self, id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT o.id, o.detection_id, d.created_at as time, o.class_id, c.name as class_name, o.accuracy "\
                            "FROM object as o "\
                            "JOIN detection d ON o.detection_id=d.id "\
                            "JOIN class c ON o.class_id=c.id "\
                            "WHERE d.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Objects associates with detection " + str(id) + " doesn't exist."}

        return jsonify(response)


# returns all objects associated with the class_id
class ObjectByClassID(Resource):
    def get(self, id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT o.id, o.detection_id, d.created_at as time, o.class_id, c.name as class_name, o.accuracy "\
                            "FROM object as o "\
                            "JOIN detection d ON o.detection_id=d.id "\
                            "JOIN class c ON o.class_id=c.id "\
                            "WHERE c.id=" + str(id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "Objects associated with class " + str(id) + " don't exist."}

        return jsonify(response)
    

class NumberOfDetectionsAndAverage(Resource):
    def get(self, timeFrame):
        SQL = "SELECT c.name, COUNT(c.name) as detections, AVG(o.accuracy) as accuracy "\
                "FROM class c "\
                "INNER JOIN object o "\
                "ON o.class_id = c.id "
                
        if ( timeFrame != 0 ):
            SQL += "WHERE o.created_at >= NOW() - INTERVAL %d HOUR " % timeFrame\

        SQL += "GROUP BY c.name "\
                "ORDER BY c.id ASC"
        
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute(SQL)
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (row_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "There are no values for that time frame."}

        return jsonify(response)

## Api resource routing here
api.add_resource(Index, '/') # index

api.add_resource(ClassList, '/class') # get a list of classes, post a new class
api.add_resource(ClassByID, '/class/<int:id>') # get user by id

api.add_resource(DetectionList, '/detection')  # get a list of detections, post a new detection, put user_confirmation and comment
api.add_resource(DetectionListUnreviewed, '/detection/unreviewed') # get a list of detections that have not recieved user feedback
api.add_resource(DetectionByID, '/detection/<int:id>/<int:raw>')  # get the image (raw (0) or with bboxes(any other value or omitted) depending on raw value) of a specific detection
api.add_resource(DetectionThumbnailByID, '/detection/<int:id>/thumbnail')  # get the image thumbnail

api.add_resource(ObjectList, '/object') # get a list of object, post a new object
api.add_resource(ObjectByID, '/object/<int:id>') # get object by id
api.add_resource(ObjectByDetectionID, '/object/detection/<int:id>') # get object by detection id
api.add_resource(ObjectByClassID, '/object/class/<int:id>') # get object by class id

## Statistics
api.add_resource(NumberOfDetectionsAndAverage, '/statistics/numberofDetectionsAndAverage/<int:timeFrame>') # get object by class id


if __name__ == '__main__':
    logging.basicConfig(filename='log_api.log', filemode='w', format='%(asctime)s - %(message)s', level=logging.INFO)
    get_classes_list() # creates a global variable with existing classes in the DB
    app.run(debug=True,host='0.0.0.0')
